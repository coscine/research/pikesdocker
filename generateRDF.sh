#!/bin/bash
CLASSPATH=pikes-tintop-1.0-SNAPSHOT-jar-with-dependencies.jar
NAF=path-to-processed-naf-folder
RDF=path-to-rdf-folder
OUTPUT_FILE=$RDF/output.tql.gz

java -cp $CLASSPATH eu.fbk.dkm.pikes.rdf.Main rdfgen $NAF -o $OUTPUT_FILE -n -m -r

#flags explanation
#  [-r,--recursive]      convert also files recursively nested in specified directories
#  [-o,--output] FILE    the output RDF file, format auto-detected (default: current directory)
#  [-i,--intermediate]   produce single RDF files (one for each input NAF) instead of a single
#                        output file; output path and format are extracted from -o argument
#  [-m,--merge]          merge instances (smushing plus filtering of group instances)
#  [-n,--normalize]      normalize/compact output so to use less metadata statements
