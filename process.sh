#!/bin/bash
CLASSPATH=pikes-tintop-1.0-SNAPSHOT-jar-with-dependencies.jar:Semafor-3.0-alpha-04.jar:models/stanford-corenlp-3.7.0-models.jar
CONF=config-pikes.prop
INPUT_FOLDER=path-to-input-naf-folder
OUTPUT_FOLDER=path-to-processed-naf-folder
PARALLEL_INSTANCES=1
MAXLENGTH=4500000
RAM=-Xmx12G

java $RAM -cp $CLASSPATH eu.fbk.dkm.pikes.tintop.FolderOrchestrator -c $CONF -i $INPUT_FOLDER -o $OUTPUT_FOLDER -s $PARALLEL_INSTANCES -z $MAXLENGTH

